var db = openDatabase("rehberDatabaseee", "1.0", "kisiler veri tabani", 2 * 1024 * 1024);

create();

function create() {
    return new Promise(function(resolve, reject) {

        db.transaction(function(tx) {
            tx.executeSql("CREATE TABLE IF NOT EXISTS KISILER (id INTEGER PRIMARY KEY,name VARCHAR(100), mail VARCHAR(100),authority VARCHAR(100),date VARCHAR(100),password VARCHAR(100))")
            resolve();
        });
    });
}

function insert(name, mail, authority, date, password) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
                tx.executeSql("INSERT INTO KISILER (name,mail,authority,date,password) VALUES(?,?,?,?,?)", [name, mail, authority, date, password]);
                resolve();
            }),
            function() {
                reject();
            }
    });
}

function select() {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("SELECT * FROM KISILER", [], function(tx, result) {
                resolve(result);
            });
        });
    });
}


function vt_delete(id) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("DELETE FROM KISILER WHERE id=?", [id]);
            resolve();
        });
    });
}

function vt_update(name, mail, authority, date, password, id) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("UPDATE KISILER SET name=? ,mail=? ,authority=?,date=? ,password=? WHERE id=?", [name, mail, authority, date, password, id]);
            resolve();
        });
    });
}

function table_delete() {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("DELETE FROM KISILER")
            resolve();
        });
    });
}

function user(mail, password) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("SELECT * FROM KISILER WHERE mail=? AND password=? ", [mail, password], function(tx, result) {
                resolve(result);
            });

        });
    });
}

function nameQuery(mail) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("SELECT name FROM KISILER WHERE mail=?", [mail], function(tx, result) {
                resolve(result);
            });
        });
    });
}

function authority(mail) {
    return new Promise(function(resolve, reject) {
        db.transaction(function(tx) {
            tx.executeSql("SELECT authority FROM KISILER WHERE mail=?", [mail], function(tx, result) {
                resolve(result);
            });
        });
    });
}